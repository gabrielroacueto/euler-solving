public class EulerEngineTest{
    public static void main(String[] args){
        
        System.out.println("Testing EulerEgine class: Expected outcome in order:");
        System.out.println("isPrime Test:");
        System.out.println("True, true, false, false");
        System.out.println();
        System.out.println(EulerEngine.isPrime(47));
        System.out.println(EulerEngine.isPrime(97));
        System.out.println(EulerEngine.isPrime(8));
        System.out.println(EulerEngine.isPrime(63));
        System.out.println();
        System.out.println("isFactor Test:");
        System.out.println("True, false, true, false");
        System.out.println();
        System.out.println(EulerEngine.isFactor(32,2));
        System.out.println(EulerEngine.isFactor(26,5));
        System.out.println(EulerEngine.isFactor(225,5));
        System.out.println(EulerEngine.isFactor(487,3));
        System.out.println("Test End.");
        
        
       
    }
}